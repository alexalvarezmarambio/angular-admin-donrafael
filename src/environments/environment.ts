import { HttpHeaders } from '@angular/common/http';


export const environment = {
  production: false,
  api: 'https://us-central1-donrafael-cb5b7.cloudfunctions.net/api',
  // api: 'http://localhost:5000/donrafael-cb5b7/us-central1/api',
  httpOptions: {
    headers: new HttpHeaders({
      Authorization: 'cXBoYXJtYTIwMjA6WA=='
    })
  },
  firebase: {
    apiKey: "AIzaSyCdrBLIVXDPnQb0BM21t-kCB743K49AFl0",
    authDomain: "donrafael-cb5b7.firebaseapp.com",
    databaseURL: "https://donrafael-cb5b7.firebaseio.com",
    projectId: "donrafael-cb5b7",
    storageBucket: "donrafael-cb5b7.appspot.com",
    messagingSenderId: "101190723772",
    appId: "1:101190723772:web:8f3b92d67f49ca8cd388b9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
