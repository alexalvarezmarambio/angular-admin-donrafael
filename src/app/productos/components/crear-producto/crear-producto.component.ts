import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ProductosService } from '../../services/productos.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef } from '@angular/material/dialog';
import { ProductoModel } from '../../../shared/models/producto.model';


@Component({
  selector: 'app-crear-producto',
  templateUrl: './crear-producto.component.html',
  styleUrls: ['./crear-producto.component.scss']
})
export class CrearProductoComponent implements OnInit {

  productoForm = this.fb.group({
    barcode: ['', Validators.required],
    name: ['', Validators.required],
    category: ['', Validators.required],
    kilo: [10, [Validators.required, Validators.min(10)]],
    precio: [10, [Validators.required, Validators.min(10)]]
  });

  abarrotes = true;

  constructor(
    private fb: FormBuilder,
    public productoService: ProductosService,
    private dialogRef: MatDialogRef<CrearProductoComponent>,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
  }

  async crearProducto() {
    try {
      const respuesta = await this.productoService.buscarProductoBarra(
        this.productoForm.controls.barcode.value.toString()
      );
      if (0 in respuesta) {
        this.mostrarSnackBar('Producto ya existe.');
        return;
      }
      const producto: ProductoModel = {
        barcode : this.productoForm.controls.barcode.value.toString(),
        category : this.productoForm.controls.category.value,
        cost : 0,
        kilo : this.productoForm.controls.kilo.value,
        margin : 0,
        name : this.productoForm.controls.name.value.toString().toUpperCase(),
        neto : 0,
        precio : this.productoForm.controls.precio.value,
        stock : 100
      };

      const response = await this.productoService.crearProducto(producto);
      this.dialogRef.close(true);

    } catch (error) {
      this.mostrarSnackBar(error.message);
    }

  }

  cambiaCategoria() {
    if(this.productoForm.controls.category.value === 'ABARROTES' ||
       this.productoForm.controls.category.value === 'PASTELERIA') {
      this.abarrotes = true;
    } else {
      this.abarrotes = false;
    }
  }

  mostrarSnackBar(mensaje: string) {
    return this.snackBar.open(mensaje, 'Cerrar', {
      duration: 3000
    });
  }


}
