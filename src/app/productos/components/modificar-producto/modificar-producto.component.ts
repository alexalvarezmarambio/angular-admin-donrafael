import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProductoModel } from 'src/app/shared/models/producto.model';
import { ProductosService } from '../../services/productos.service';


@Component({
  selector: 'app-modificar-producto',
  templateUrl: './modificar-producto.component.html',
  styleUrls: ['./modificar-producto.component.scss']
})
export class ModificarProductoComponent implements OnInit {

  productoForm = this.fb.group({
    name: [this.producto.name, Validators.required],
    category: [this.producto.category, Validators.required],
    precio: [this.producto.precio, [Validators.required, Validators.min(10)]],
    kilo: [this.producto.kilo, [Validators.required, Validators.min(10)]]
  });

  abarrotes = true;

  constructor(
    private dialogRef: MatDialogRef<ModificarProductoComponent>,
    @Inject(MAT_DIALOG_DATA) public producto: ProductoModel,
    public productoService: ProductosService,
    private fb: FormBuilder,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.cambiarAbarrotes();
    this.cambiarKilo();
  }

  async modificarProducto() {
    try {
      const body = {
        name: this.productoForm.controls.name.value.toString().toUpperCase(),
        category: this.productoForm.controls.category.value,
        precio: this.productoForm.controls.precio.value,
        kilo: this.productoForm.controls.kilo.value
      };
      await this.productoService.modificarProducto(this.producto.id, body);
      this.dialogRef.close(true);
    } catch (error) {
      this.mostrarSnackBar(error.message);
    }
  }

  cambiarKilo() {
    if (this.productoForm.controls.kilo.value === 0) {
      this.productoForm.controls.kilo.setValue(10);
    }
  }

  cambiarAbarrotes() {
    if (this.productoForm.controls.category.value === 'ABARROTES' ||
        this.productoForm.controls.category.value === 'PASTELERIA') {
      this.abarrotes = true;
    } else {
      this.abarrotes = false;
    }
  }

  mostrarSnackBar(mensaje: string) {
    return this.snackbar.open(mensaje, 'Cerrar', {
      duration: 3000
    })
  }

}
