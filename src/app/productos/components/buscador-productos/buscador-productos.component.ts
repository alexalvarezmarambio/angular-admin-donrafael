import { AfterContentChecked, AfterContentInit, AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ProductoModel } from 'src/app/shared/models/producto.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProductosService } from '../../services/productos.service';
import { MatDialog } from '@angular/material/dialog';
import { CrearProductoComponent } from '../crear-producto/crear-producto.component';
import { MatInput } from '@angular/material/input';
import { ModificarProductoComponent } from '../modificar-producto/modificar-producto.component';
import { ConfirmComponent } from 'src/app/shared/components/confirm/confirm.component';
import { ExcelService } from 'src/app/shared/service/excel.service';



@Component({
  selector: 'app-buscador-productos',
  templateUrl: './buscador-productos.component.html',
  styleUrls: ['./buscador-productos.component.scss']
})
export class BuscadorProductosComponent implements OnInit {

  cargando = false;
  columnas: string[] = ['barra', 'name', 'category', 'acciones'];
  dataSource: MatTableDataSource<ProductoModel>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) input: MatInput;

  constructor(
    public productoService: ProductosService,
    public excelService: ExcelService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.buscarProductos();
  }

  ngOnInit(): void {}

  async buscarProductos() {
    try {
      this.cargando = true;
      const response = await this.productoService.buscarProductos();
      console.log('productos', response);
      this.dataSource = new MatTableDataSource(response);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.cargando = false;

    } catch (error) {
      this.mostrarSnackBar(error.message);
      this.cargando = false;
    }
  }

  recargar() {
    this.input.value = '';
    this.buscarProductos();
  }

  aplicarFiltro(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  async crearProducto() {
    try {
      const dialogRef = this.dialog.open(CrearProductoComponent, {
        hasBackdrop: true,
        disableClose: true
      });

      const respuesta = await dialogRef.afterClosed().toPromise();
      if (respuesta) {
        this.mostrarSnackBar('Producto Creado.');
        this.recargar();
      }
    } catch (error) {
      this.mostrarSnackBar(error.message);
    }

  }

  async modificarProducto(producto: ProductoModel) {
    try {
      const dialogRef = this.dialog.open(ModificarProductoComponent, {
        disableClose: true,
        hasBackdrop: true,
        data: producto
      })

      const respuesta = await dialogRef.afterClosed().toPromise();
      if (respuesta) {
        this.mostrarSnackBar('Producto Actualizado.');
        this.recargar();
      }
    } catch (error) {
      this.mostrarSnackBar(error.message);
    }
  }

  async eliminarProducto(id: string) {
    try {
      const dialogRef = this.dialog.open(ConfirmComponent, {
        hasBackdrop: true,
        disableClose: false
      });

      const respuesta = await dialogRef.afterClosed().toPromise();
      if (respuesta) {
        const response = await this.productoService.eliminarProducto(id);
        this.mostrarSnackBar('Producto Eliminado.');
        this.recargar();
      }
    } catch (error) {
      this.mostrarSnackBar(error.message);
    }
  }

  exportarExcel() {
    this.excelService.exportAsExcelFile(this.dataSource.data, 'productos_donrafael');
  }

  mostrarSnackBar(mensaje: string) {
    this.snackBar.open(mensaje, 'Cerrar', {
      duration: 3000
    });
  }

}
