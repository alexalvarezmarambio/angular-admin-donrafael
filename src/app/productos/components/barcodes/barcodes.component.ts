import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Observable} from 'rxjs';
import {ProductoModel} from 'src/app/shared/models/producto.model';
import {ProductosService} from '../../services/productos.service';

@Component({
  selector: 'app-barcodes',
  templateUrl: './barcodes.component.html',
  styleUrls: ['./barcodes.component.scss']
})
export class BarcodesComponent implements OnInit {

  tabla: any[] = [];

  constructor(
    private snackBar: MatSnackBar,
    public productoService: ProductosService
  ) { }

  ngOnInit(): void {
  }

  buscar(categoria: string) {
    // this.productos = this.productoService.buscarProductoCategoria(categoria);
    this.tabla = [];
    this.productoService.buscarProductoCategoria(categoria).subscribe((productos: ProductoModel[]) => {
      let row: ProductoModel[] = [];
      let cont = 0;
      let largo = 0;

      productos.forEach((producto: ProductoModel) => {
        if (cont < 3) {
          row.push(producto);
          cont++;
          largo++;
        }

        if (cont === 3 || largo === productos.length) {
          this.tabla.push(row);
          row = [];
          cont = 0
        }
      });
    });
  }

  print() {
    if (this.tabla.length === 0) {
      this.mostrarSnackBar('Debe Cargar una Categoria.');
    }
  }

  mostrarSnackBar(mensaje: string) {
    return this.snackBar.open(mensaje, 'Cerrar', {
      duration: 3000
    });
  }



}
