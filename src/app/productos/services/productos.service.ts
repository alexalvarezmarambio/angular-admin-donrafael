import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import { ProductoModel } from 'src/app/shared/models/producto.model';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(
    private http: HttpClient,
    private afs: AngularFirestore
  ) { }

  buscarProductos() {
    return this.http.get<ProductoModel[]>('/producto').toPromise();
  }

  buscarProductoBarra(barra: string) {
    return this.http.get<ProductoModel[]>(`/producto/${ barra }`).toPromise();
  }

  buscarProductoCategoria(categoria: string): Observable<ProductoModel[]> {
    return this.afs.collection<ProductoModel>('products', ref => ref.where('category', '==', categoria)).valueChanges({idField: 'id'});
  }

  crearProducto(producto: ProductoModel) {
    return this.http.post('/producto', producto).toPromise();
  }

  modificarProducto(id: string, body: any) {
    return this.http.put(`/producto/${ id }`, body).toPromise();
  }

  eliminarProducto(id: string) {
    return this.http.delete(`/producto/${ id }`).toPromise();
  }
}
