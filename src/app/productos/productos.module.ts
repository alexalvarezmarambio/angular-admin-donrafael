import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductosRoutingModule } from './productos-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

// Material
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';

import { NgxPrintModule } from 'ngx-print';
import { NgxBarcodeModule } from 'ngx-barcode';

// Components
import { BuscadorProductosComponent } from './components/buscador-productos/buscador-productos.component';
import { CrearProductoComponent } from './components/crear-producto/crear-producto.component';
import { ModificarProductoComponent } from './components/modificar-producto/modificar-producto.component';
import { BarcodesComponent } from './components/barcodes/barcodes.component';


@NgModule({
  declarations: [BuscadorProductosComponent, CrearProductoComponent, ModificarProductoComponent, BarcodesComponent],
  imports: [
    CommonModule,
    ProductosRoutingModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSelectModule,
    ReactiveFormsModule,
    SharedModule,
    NgxPrintModule,
    NgxBarcodeModule
  ]
})
export class ProductosModule { }
