import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BarcodesComponent} from './components/barcodes/barcodes.component';
import { BuscadorProductosComponent } from './components/buscador-productos/buscador-productos.component';

const routes: Routes = [
  {path: '', redirectTo: 'buscador', pathMatch: 'full'},
  {path: 'buscador', component: BuscadorProductosComponent},
  {path: 'barcodes', component: BarcodesComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductosRoutingModule { }
