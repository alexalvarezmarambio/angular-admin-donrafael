import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-realce';

  constructor(
    private afa: AngularFireAuth
  ) {}

  ngOnInit() {
    this.afa.signInAnonymously();
  }
}
