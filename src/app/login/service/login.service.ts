import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UsuarioModel } from 'src/app/shared/models/usuario.model';
import {UsuarioLogin} from '../login/login.component';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http: HttpClient
  ) { }

  auth(usuarioLogin: UsuarioLogin) {
    return this.http.post<UsuarioModel[]>('/usuario/login', usuarioLogin).toPromise();
  }
}
