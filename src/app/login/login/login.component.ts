import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../service/login.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MainService } from 'src/app/shared/service/main.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = this.fb.group({
    usuario: ['', Validators.required],
    clave: ['', Validators.required]
  });

  constructor(
    public mainService: MainService,
    public loginService: LoginService,
    private router: Router,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    // this.loginExpress();
  }

  async login() {
    try {

      const body: UsuarioLogin = {
        usuario: this.loginForm.controls.usuario.value,
        clave: this.loginForm.controls.clave.value
      };

      const response = await this.loginService.auth(body);
      if (!(0 in response)) {
        this.mostrarSnackBar('Usuario no Existe.')
        return;
      }
      this.mainService.usuario = response;
      this.mainService.auth = true;
      this.router.navigate(['/layout']);
    } catch (error) {
      this.mostrarSnackBar(error.message);
    }

  }

  async loginExpress() {
    const body: UsuarioLogin = {
      usuario: 'admin',
      clave: '1234'
    };

    const response = await this.loginService.auth(body);
    this.mainService.usuario = response;
    this.mainService.auth = true;
    this.router.navigate(['/layout/productos/barcodes']);
  }

  mostrarSnackBar(mensaje: string) {
    this.snackBar.open(mensaje, 'Cerrar', {
      duration: 3000
    });
  }

}

export interface UsuarioLogin {
  usuario: string,
  clave: string;
}
