export interface UsuarioModel {
  id?: string;
  usuario: string;
  clave: string;
  nombre: string;
  email: string;
}
