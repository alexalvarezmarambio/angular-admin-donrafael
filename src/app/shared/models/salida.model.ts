import {Proveedor} from './proveedor.model';

export interface Salida {
  proveedor: Proveedor;
  monto: number;
}
