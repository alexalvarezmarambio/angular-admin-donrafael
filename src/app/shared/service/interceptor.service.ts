import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable, throwError} from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const { api, httpOptions } = environment;

    const reqClonada = req.clone({
      url: api + req.url,
      headers: httpOptions.headers
    });

    return next.handle(reqClonada).pipe(
      catchError(this.manejaError)
    );
  }

  manejaError(error: HttpErrorResponse) {
    return throwError(error.message);
  }

}
