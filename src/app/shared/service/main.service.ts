import { Injectable } from '@angular/core';
import { UsuarioModel } from 'src/app/shared/models/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  usuario: UsuarioModel[] = [];
  auth = false;

  constructor() { }
}
