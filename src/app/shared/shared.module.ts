import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Material
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

// Components
import { ConfirmComponent } from './components/confirm/confirm.component';

@NgModule({
  declarations: [
    ConfirmComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule
  ],
  exports: [
    ConfirmComponent
  ]
})
export class SharedModule { }
