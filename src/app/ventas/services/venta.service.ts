import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {VentaModel} from 'src/app/shared/models/venta.model';

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  constructor(
    private http: HttpClient
  ) { }

  obtenerVentas(desde: string, hasta: string) {
    return this.http.get<VentaModel[]>(`/ventas/${ desde }/${ hasta }`).toPromise();
  }

  obtnerVentasCaja(id: string) {
    return this.http.get<VentaModel[]>(`/ventas/${ id }`).toPromise();
  }
}
