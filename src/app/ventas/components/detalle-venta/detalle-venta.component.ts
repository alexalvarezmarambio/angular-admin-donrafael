import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {ProductoModel} from 'src/app/shared/models/producto.model';

@Component({
  selector: 'app-detalle-venta',
  templateUrl: './detalle-venta.component.html',
  styles: [
  ]
})
export class DetalleVentaComponent implements OnInit {
  
  dataSource: MatTableDataSource<ProductoModel>;
  columnas = ['name', 'cantidad', 'precio', 'total'];

  constructor(
    @Inject(MAT_DIALOG_DATA) public productos: ProductoModel[]
  ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.productos);
  }

}
