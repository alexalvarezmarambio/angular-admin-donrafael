import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {VentaService} from '../../services/venta.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import * as moment from 'moment'
import {VentaModel} from 'src/app/shared/models/venta.model';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {ProductoModel} from 'src/app/shared/models/producto.model';
import {DetalleVentaComponent} from '../detalle-venta/detalle-venta.component';

@Component({
  selector: 'app-lista-ventas',
  templateUrl: './lista-ventas.component.html',
  styles: [
  ]
})
export class ListaVentasComponent implements OnInit {

  total = 0;

  busquedaForm = this.fb.group({
    desde: ['', Validators.required],
    hasta: ['', Validators.required]
  });

  dataSource: MatTableDataSource<VentaModel>;
  columnas = ['fecha', 'total', 'dte', 'folio', 'vendedor', 'acciones'];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private fb: FormBuilder,
    public ventaService: VentaService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  async buscarVentas() {

    this.total = 0;

    const desdeForm = this.busquedaForm.controls.desde.value;
    const hastaForm = this.busquedaForm.controls.hasta.value;

    const desde = moment(desdeForm).format('YYYY-MM-DD');
    const hasta = moment(hastaForm).format('YYYY-MM-DD');

    try {
      const response = await this.ventaService.obtenerVentas(desde, hasta);
      this.dataSource = new MatTableDataSource(response);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.total = this.sumarTotal();
    } catch (error) {
      this.mostrarSnackBar(error.message);
    }
  }

  sumarTotal () {
    return this.dataSource.data.reduce( (acumulador, venta) => acumulador + venta.total, this.total );
  }

  mostrarDetalleVenta(productos: ProductoModel[]) {
    this.dialog.open(DetalleVentaComponent, {
      data: productos,
      width: '60vw'
    });
  }

  mostrarSnackBar(mensaje: string) {
    return this.snackBar.open(mensaje, 'Cerrar', {
      duration: 3000
    });
  }

}
