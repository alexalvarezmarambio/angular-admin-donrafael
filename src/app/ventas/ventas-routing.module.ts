import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListaVentasComponent} from './components/lista-ventas/lista-ventas.component';

const routes: Routes = [
  {path: '', redirectTo: 'lista', pathMatch: 'full'},
  {path: 'lista', component: ListaVentasComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VentasRoutingModule { }
