import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ConfirmComponent} from 'src/app/shared/components/confirm/confirm.component';
import {CajaModel} from 'src/app/shared/models/caja.model';
import {CrearCajaComponent} from '../crear-caja/crear-caja.component';
import {CajaService} from '../../service/caja.service';
import {CuadreComponent} from '../cuadre/cuadre.component';

@Component({
  selector: 'app-lista-cajas',
  templateUrl: './lista-cajas.component.html',
  styles: [
  ]
})
export class ListaCajasComponent implements OnInit {

  cajas: CajaModel[] = [];

  constructor(
    public cajaService: CajaService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.obtenerCajas();
  }

  async obtenerCajas() {
    try {
      const response = await this.cajaService.obtenerCajasActivas();
      this.cajas = response;

    } catch (error) {
      this.mostrarSnackBar(error.message);
    }
  }

  async crearCaja() {
    try {
      const dialogRef = this.dialog.open(CrearCajaComponent, {
        hasBackdrop: true,
        disableClose: true
      });

      const respuesta = await dialogRef.afterClosed().toPromise();
      if (respuesta) {
        const snackRef = this.mostrarSnackBar('Caja Creada');
        await snackRef.afterDismissed().toPromise();
        this.obtenerCajas();
      }
    } catch (error) {
      this.mostrarSnackBar(error.message);
    }
  }

  async actualizarEstado(id: string, estado: string) {
    try {
      const dialogRef = this.dialog.open(ConfirmComponent);
      const respuesta = await dialogRef.afterClosed().toPromise();
      if (respuesta) {
        await this.cajaService.acutalizarEstado(id, estado);
        const mensaje = estado === '0' ? 'Caja Abierta' : 'Caja Cerrada';
        this.mostrarSnackBar(mensaje);
        this.obtenerCajas();
      }

    } catch (error) {
      this.mostrarSnackBar(error.message);
    }
  }

  async detalleCaja(id: string) {
    try {
      const response = await this.cajaService.obtenerCajasActivas();
      console.log(response);
    } catch (error) {
      this.mostrarSnackBar(error.message);
    }
  }

  async abrirDialogoCuadre(caja: CajaModel) {
    try {
      const dialogRef = this.dialog.open(CuadreComponent, {
        disableClose: true,
        width: '50vw',
        data: caja
      });
      const respuesta = await dialogRef.afterClosed().toPromise();
      if (respuesta) {
        this.mostrarSnackBar('Caja Cuadrada.')
        this.obtenerCajas();
      }
    } catch (error) {
      this.mostrarSnackBar(error.message);
    }
  }

  mostrarSnackBar(mensaje: string) {
    return this.snackBar.open(mensaje, 'Cerrar', {
      duration: 3000
    })
  }


}
