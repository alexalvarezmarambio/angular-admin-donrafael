import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ConfirmComponent} from 'src/app/shared/components/confirm/confirm.component';
import {CajaModel} from 'src/app/shared/models/caja.model';
import {CuadreModel} from 'src/app/shared/models/cuadre.model';
import {VentaModel} from 'src/app/shared/models/venta.model';
import {VentaService} from 'src/app/ventas/services/venta.service';
import {CajaService} from '../../service/caja.service';

@Component({
  selector: 'app-cuadre',
  templateUrl: './cuadre.component.html',
  styles: [
  ]
})
export class CuadreComponent implements OnInit {

  cuadre: CuadreModel = {
        ventas: 0,
        salidas: 0,
        remesas: 0,
        sistema: 0,
        vendedor: 0,
        diferencia: 0
      };

  constructor(
    private dialogRef: MatDialogRef<CuadreComponent>,
    @Inject(MAT_DIALOG_DATA) public caja: CajaModel,
    private snackBar: MatSnackBar,
    public ventaService: VentaService,
    public cajaService: CajaService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.obtenerVentasCaja();
  }

  async obtenerVentasCaja() {
    try {
      const response = await this.ventaService.obtnerVentasCaja(this.caja.id);
      this.cuadre.ventas = this.sumarVentas(response);
      this.cuadre.remesas = this.sumarRemesas();
      this.cuadre.salidas = this.sumarSalidas();
      this.cuadre.sistema = this.sumarSistema();
      this.cuadre.vendedor = this.cuadre.remesas;
      this.cuadre.diferencia = this.sumarDiferencia();

    } catch (error) {
      this.mostrarSnackBar(error.message);
    }
  }

  async acutalizarCuadre() {
    try {
      const dialogRef = this.dialog.open(ConfirmComponent);
      const respuesta = await dialogRef.afterClosed().toPromise();
      if (respuesta) {
        const promesas = [
          this.cajaService.actualizarCuadre(this.caja.id, this.cuadre),
          this.cajaService.acutalizarEstado(this.caja.id, '2')
        ];

        await Promise.all(promesas);
        this.dialogRef.close(true);  
      }
      

    } catch (error) {
      this.mostrarSnackBar(error.message);
    }
  }

  sumarDiferencia() {
    return this.cuadre.vendedor - this.cuadre.sistema;
  }

  sumarVendedor() {
    return this.cuadre.remesas - this.caja.fondo - this.cuadre.salidas;
  }

  sumarSistema() {
    return this.caja.fondo + this.cuadre.ventas - this.cuadre.salidas;
  }

  sumarVentas(ventas: VentaModel[]) {
    return ventas.reduce((acumulator, value) => acumulator + value.total, this.cuadre.ventas);
  }

  sumarSalidas() {
    return this.caja.salidas.reduce((acumulator, value) => acumulator + value.monto, this.cuadre.salidas);
  }

  sumarRemesas() {
    return this.caja.remesas.reduce((acumulator, value) => acumulator + value.monto, this.cuadre.remesas);
  }

  mostrarSnackBar(mensaje: string) {
    return this.snackBar.open(mensaje, 'Cerrar', {
      duration: 3000
    });
  }

}
