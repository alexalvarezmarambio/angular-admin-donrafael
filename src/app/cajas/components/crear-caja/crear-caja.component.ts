import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {VendedorModel} from 'src/app/shared/models/vendedor.model';
import * as moment from 'moment';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CajaService} from '../../service/caja.service';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-crear-caja',
  templateUrl: './crear-caja.component.html',
  styles: [
  ]
})
export class CrearCajaComponent implements OnInit {

  cajaForm = this.fb.group({
    apertura: [moment().format('YYYY-MM-DD HH:mm:ss')],
    cierre: [''],
    fondo: ['', Validators.required],
    estado: ['0'],
    vendedor: ['', Validators.required],
    remesas: [[]],
    salidas: [[]]
  });

  vendedores: VendedorModel[] = [
    {
      id: '05LNgbylRm1xwM0SAWh3',
      nombre: 'NAIDA',
      clave: '1962'
    },
    {
      id: 'evp6Is8YwUK2dc0gAywE',
      nombre: 'ABELMARY',
      clave: '1962'
    }
  ];

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    public cajaService: CajaService,
    private dialogRef: MatDialogRef<CrearCajaComponent>
  ) { }

  ngOnInit(): void {
  }

  async crearCaja() {
    try {
      const vendedor = this.cajaForm.controls.vendedor.value;
      const cajas = await this.cajaService.obtenerCajaClave(vendedor.clave);
      if (0 in cajas) {
        this.mostrarSnackBar('Caja Abierta.');
        return;
      }

      await this.cajaService.crearCaja(this.cajaForm.value);
      this.dialogRef.close(true);
    } catch (error) {
      this.mostrarSnackBar(error.message);
    }
  }

  mostrarSnackBar(mensaje: string) {
    return this.snackBar.open(mensaje, 'Cerrar', {
      duration: 3000
    });
  }

}
