import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {CajaModel} from 'src/app/shared/models/caja.model';
import {CuadreModel} from 'src/app/shared/models/cuadre.model';

@Injectable({
  providedIn: 'root'
})
export class CajaService {

  constructor(
    private http: HttpClient
  ) { }

  obtenerCajasActivas() {
    return this.http.get<CajaModel[]>('/cajas/activas').toPromise();
  }

  obtenerCajaClave(clave: string) {
    return this.http.get<CajaModel[]>(`/cajas/clave/${ clave }`).toPromise();
  }

  crearCaja(caja: CajaModel) {
    return this.http.post('/cajas', caja).toPromise();
  }

  acutalizarEstado(id: string, estado: string) {
    return this.http.put(`/cajas/estado/${ id }/${ estado }`, null).toPromise();
  }

  actualizarCuadre(id: string, cuadre: CuadreModel) {
    return this.http.put(`/cajas/cuadre/${ id }`, cuadre).toPromise();
  }
}
