import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListaCajasComponent} from './components/lista-cajas/lista-cajas.component';

const routes: Routes = [
  {path: '', redirectTo: 'lista', pathMatch: 'full'},
  {path: 'lista', component: ListaCajasComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CajasRoutingModule { }
