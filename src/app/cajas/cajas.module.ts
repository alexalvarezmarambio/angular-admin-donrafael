import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CajasRoutingModule } from './cajas-routing.module';

// Material
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';

import { CrearCajaComponent } from './components/crear-caja/crear-caja.component';
import { CuadreComponent } from './components/cuadre/cuadre.component';
import { ListaCajasComponent } from './components/lista-cajas/lista-cajas.component';

@NgModule({
  declarations: [ListaCajasComponent, CrearCajaComponent, CuadreComponent],
  imports: [
    CommonModule,
    CajasRoutingModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatListModule
  ]
})
export class CajasModule { }
