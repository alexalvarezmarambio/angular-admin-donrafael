import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LayoutComponent } from './components/layout/layout.component';

const routes: Routes = [
  {path: '', component: LayoutComponent, children: [
    {
      path: 'dashboard', component: DashboardComponent
    },
    {
      path: '', redirectTo: 'dashboard', pathMatch: 'full'
    },
    {
      path: 'productos',
      loadChildren: () => import('../productos/productos.module').then(m => m.ProductosModule)
    },
    {
      path: 'cajas',
      loadChildren: () => import('../cajas/cajas.module').then(m => m.CajasModule)
    },
    {
      path: 'ventas',
      loadChildren: () => import('../ventas/ventas.module').then(m => m.VentasModule)
    }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
