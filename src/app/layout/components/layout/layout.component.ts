import { Component, OnInit, ViewChild } from '@angular/core';
import { SidebarComponent } from '../sidebar/sidebar.component';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  @ViewChild(SidebarComponent)
  sideBar!: SidebarComponent;

  constructor() { }

  ngOnInit(): void {
  }

  abrirMenu(mensaje: string) {
    console.log('Message: ', mensaje);
    this.sideBar.toggleSideNav();
  }

}
