import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { MainService } from 'src/app/shared/service/main.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @ViewChild('menu')
  menu!: MatSidenav;

  constructor(
    public mainService: MainService
  ) { }

  ngOnInit(): void {
  }

  toggleSideNav() {
    this.menu.toggle();
  }

}
