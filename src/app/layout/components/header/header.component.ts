import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MainService } from 'src/app/shared/service/main.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() open = new EventEmitter<string>();

  constructor(
    public mainService: MainService
  ) { }

  ngOnInit(): void {
  }

  abrirMenu() {
    this.open.emit('Abrir Menu.');
  }

}
